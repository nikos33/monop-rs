use crate::game::Game;


pub fn check_conditions_for_step(g: &mut Game)->u8 {
    //let pl =  g.curr_pl();

    if g.curr_pl().bot && g.curr_pl().police>0 && move_away_from_jail(g) {
        let pl =  g.curr_pl();
        pl.money-=500;
        pl.police=0;
        g.log("_paid_500_and_go_from_jail".to_string());
    }

    if g.curr_pl().police>0{
        if let 11|22|33|44|55|66 = g.last_roll {
            g.curr_pl().police=0;
            g.log("_you_released_from_jail_because_of_double_roll".to_string());
        }else{
            let pl =  g.curr_pl();
            pl.police+=1;
            if pl.police==4{
                g.log("you must pay $500 to go from jail".to_string());
                g.to_pay(500,false);
                return 1;
            }else{
                g.log("you skip turn".to_string());
            }
        }
    }
    if is_tripple(g){
        g.log("_go_jail_after_trippe".to_string());
        g.curr_pl().pos=10;
        g.curr_pl().police=1;
        return 2;
    }
    return 0
}
fn is_tripple(g: &Game)-> bool {
    return false
}
fn move_away_from_jail(g: &Game)-> bool {
  return true
}

pub fn process_position(g: &mut Game) {
    let pl = g.curr_pl();
    let cell = g.cells[pl.pos];

    match cell.ctype{
        1|2|3 => process_land(g),
        6 => pay_tax(g),
        4 => process_random(g),
        _ => (),
    }
    if pl.pos ==30 {
        pl.pos=10;
        pl.police=1;
        g.log("_move_to_jail_after_30".to_string());
        g.finish_step();
    }
}

fn process_land(g: &Game){
}
fn pay_tax(g: &Game){
}
fn process_random(g: &Game){
}