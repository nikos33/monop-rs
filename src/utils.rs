pub mod file_utils {
    use crate::entites::Cell;
    use std::fs::File;
    use std::io::{BufRead, BufReader, Result};

    pub fn init_game_messages() {}

    pub fn init_cells_from_file() -> Vec<Cell> {
        let file = File::open("data/lands_en.txt").unwrap();
        let mut cells: Vec<Cell> = Vec::with_capacity(40);

        let parse_int8 = |text: &str| -> i8 {
            match text.trim().parse::<i8>() {
                Err(e) => -1,
                Ok(id) => id,
            }
        };
        let parse_int16 = |text: &str| -> i16 {
            match text.trim().parse::<i16>() {
                Err(e) => -1,
                Ok(id) => id,
            }
        };

        for (i, line) in BufReader::new(file).lines().enumerate() {
            if i == 0 {
                continue;
            }
            let lline = line.unwrap();
            if !lline.is_empty() {
                let arr: Vec<&str> = lline.split("|").collect();
                let id = parse_int8(arr[1]);
                let cost = parse_int16(arr[2]);
                let ctype = parse_int8(arr[3]);
                let group = parse_int8(arr[4]);

                cells.push(Cell {
                    id: id,
                    title: arr[0].trim().to_string(),
                    cost: cost,
                    ctype: ctype,
                    group: group,
                    owner: 0,
                    rent_info: arr[5].trim().to_string(),
                    info: arr[6].trim().to_string(),
                });
            }
        }
        cells
    }

}
