//extern crate monop;

use monop::game::Game;
use monop::entites::Player;
use std::mem;

fn main() {
    let id = 2;
    let lang:i8 = 1; //ru
    
    let mut g = Game::new(id, lang);
    let p1 = Player::new(1, String::from("kilk"), false);
    let p2 = Player::new(2, String::from("bot_1"), true);
    g.add_player(p1);
    g.add_player(p2);

    g.print();
    g.start();
    for rr in 0..20{
      g.roll_and_step();
    }
    //g.print_cells(); 
    //println!("bytes in the stack {}", mem::size_of_val(&g));
}
