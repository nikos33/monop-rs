//mod entites;
//extern crate monop

use crate::entites::Cell;
use crate::entites::Player;
use crate::utils::file_utils;
use crate::manager::player_steps_manager;
use crate::manager::player_manager;

extern crate rand;
use rand::Rng;

#[derive(PartialEq)]
enum GState {
    Start,
    BeginStep,
    CanBuy,
    Auction,
    Trade,
    NeedPay,
    CantPay,
    EndStep,
}

pub struct Game {
    id: i16,
    round: i16,
    pub last_roll: usize,
    manual_roll: usize,
    pub cells: Vec<Cell>,
    players: Vec<Player>,
    log_messages: Vec<String>,
    log_rounds: Vec<RoundLogMessage>,
    state: GState,
    lang: i8, //0-eng, 1-ru
    is_manual_roll_mode: bool,
    need_show_log: bool,
    selected: usize,
    selected_pos: i8,
}
pub struct RoundLogMessage {
    r: i16,
    pid: usize,
    roll: usize,
}
impl Game {
    pub fn new(id: i16, lang: i8) -> Game {
        Game {
            id: id,
            is_manual_roll_mode: false,
            need_show_log: true,
            lang: lang,
            round: 0,
            last_roll: 0,
            manual_roll: 0,
            selected: 0,
            selected_pos:0,
            players: Vec::with_capacity(4),
            log_messages: Vec::with_capacity(200),
            log_rounds: Vec::with_capacity(200),
            cells: file_utils::init_cells_from_file(),
            state: GState::Start,
        }
    }
    pub fn add_player(&mut self, p: Player) {
        self.players.push(p);
    }
    pub fn print(&self) {
        println!("game: id {}, lang:{}", self.id, self.lang);
        //println!("pl_id {}, pl_pos:{}", self.players[self.selected].id, self.players[self.selected].pos);
    }
    pub fn log(&mut self, text: String) {
        self.log_messages.push(text);
    }

    pub fn print_cells(&self) {
        for cell in &self.cells {
            println!(
                "cid:{} title:{} type:{} group:{}",
                cell.id, cell.rent_info, cell.ctype, cell.group
            );
        }
    }
    pub fn curr_pl(&mut self)-> &mut Player {
        &mut self.players[self.selected]
    }
    pub fn get_cell(&mut self, index: usize)-> &mut Cell {
        &mut self.cells[index]
    }
    pub fn get_ctype(&self, index: usize)-> i8 {
        self.cells[index].ctype
    }
    pub fn change_curr_pos(&mut self) {
        let last = self.players[self.selected].pos;
        let step = self.last_roll/10 + self.last_roll%10;
        if last+ step > 39 {
            self.players[self.selected].pos = (last+ step) % 40;
            self.change_curr_money(2000);
            if self.need_show_log { println!("round:{}_{} --add money for passing start 2000", self.round, self.selected); }
        } else {
            self.players[self.selected].pos = last+ step;
        }
    }

    pub fn change_curr_money(&mut self,  money: i32) {
        self.players[self.selected].money += money;
    }

    pub fn start(&mut self) {
        self.state = GState::BeginStep;
        self.round = 1;
        self.selected = 0;
    }
    pub fn finish_step(&mut self) {
        // action when finishing
        self.finish_round();
    }
    pub fn finish_round(&mut self) {
        self.state = GState::BeginStep;
        self.round += 1;
        match self.last_roll{
            11|22|33|44|55|66 => (),
            _ => self.selected += 1,
        }

        if self.selected >= self.players.len() {
            self.selected = 0;
        }
    }
    pub fn to_pay(&mut self, amount: i32, finish_step: bool) {
        self.state = GState::NeedPay;
        if self.curr_pl().bot{
            player_manager::pay(self, finish_step)
        }
    }

    pub fn to_aucton(finish_step: bool) {
    }

    pub fn roll_and_step(&mut self) {
        self.make_roll();
        self.log_rounds.push(RoundLogMessage{r:self.round, pid:self.selected, roll: self.last_roll});

        if !self.is_manual_roll_mode {
            let res = player_steps_manager::check_conditions_for_step(self);
            match res{
              0 => self.make_step(),
              1 => println!("pay 500 and go away from jail"),
              2 => println!("goto jail after trippe"),
              _ => (),
            }
        }else{
        }
        self.finish_round();
    }
    pub fn make_roll(&mut self) {
        let mut rng = rand::thread_rng();
        let r1 = rng.gen_range(1, 7);
        let r2 = rng.gen_range(1, 7);

        self.last_roll = r1 * 10 + r2;
        match self.last_roll{
            11|22|33|44|55|66 => self.curr_pl().double_roll += 1,
            _ => self.curr_pl().double_roll = 0,
        }
    }
    pub fn make_step(&mut self) {
        if self.state != GState::BeginStep {
            return ();
        }
        let old_pos = self.curr_pl().pos;
        self.change_curr_pos();
        let new_pos = self.curr_pl().pos;
        if self.need_show_log{
          println!("round:{}_{} --make_step roll:{}  new_pos:{}->{}",self.round, self.selected,  self.last_roll, old_pos, new_pos);
        }
    }


}
