//pub mod player;

pub struct Cell {
    pub id: i8,
    pub cost: i16,
    pub ctype: i8,
    pub group: i8,
    pub owner: i8,
    pub title: String,
    pub rent_info: String,
    pub info: String,
}
pub struct Player {
    pub id: i8,
    pub name: String,
    pub bot: bool,
    pub money: i32,
    pub pos: usize,
    pub last_roll: i8,
    pub manual_roll: i8,
    pub police: i8,
    pub police_key: i8,
    pub double_roll: i8,
}

struct Trade {}

struct Auction {
    cell_id: i8,
    curr_bid: i16,
    curr_pl: i8,
    last_bidded_player: i8,
    finished: bool,
    auc_pls: [i8; 4],
}

struct ChestCard {
    ctype: i8,
    r_group: i8,
    text: String,
    money: i32,
    pos: i8,
}

impl Player {
    pub fn new(id: i8, name: String, isbot: bool ) -> Player {
        Player {
            id: id,
            name: name,
            bot: isbot,
            money:0,
            pos:0,
            last_roll: 0,
            manual_roll: 0,
            police: 0,
            police_key: 0,
            double_roll:0,
        }
    }
}

impl Cell{
    pub fn is_land(&self)-> bool{
        match self.ctype{
            1|2|3 => true,
            _ => false,
        }
    }
    pub fn get_rent_payment(&self, ind: usize)-> i16{
        let data:Vec<&str> = self.rent_info.split(";").collect();
        match data[ind].parse::<i16>() {
            Err(e) => 0,
            Ok(amount) => amount,
        }

    }
}